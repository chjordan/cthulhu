#!/usr/bin/env python2

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# Python 2 and 3 compatibility
from __future__ import print_function, division

import sys
import copy
import argparse
import multiprocessing as mp

import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams.update({'font.size': 22})

from cthulhu.unpack import unpack_data
from cthulhu.reconstruct import Obsid
from cthulhu.analyses import temporal_correlation


parser = argparse.ArgumentParser()
parser.add_argument("-p", "--plotting", action="store_true",
                    help="Plot the resulting temporal correlation.")
parser.add_argument("files", nargs='*', help="Files to be processed. " \
                    "See cthulhu.unpack_data for appropriate formats.")
args = parser.parse_args()


def unpack_to_dict(filename):
    unpacked = unpack_data(filename)
    obs = Obsid(unpacked, filename)
    obs.sources = unpacked["sources"]
    return obs


if __name__ == "__main__":
    obs_objects = mp.Pool().map(unpack_to_dict, args.files)
    data = {}
    for obj in obs_objects:
        data[obj.obsid] = obj
    obsids = np.array(sorted(data.keys()))
    rho, errors = temporal_correlation(data)

    # Plotting.
    if args.plotting:
        x, y = [], []
        for t in sorted(rho.keys()):
            print(t, rho[t], errors[t])
            x.append(t)
            y.append(rho[t])

        plt.figure(figsize=(10, 10))
        plt.errorbar(x, y, yerr=errors, lw=3, elinewidth=3, ecolor='r', barsabove=True)
        plt.grid(True)

        plt.xlim(0, max(x))
        plt.xlabel("Lag (seconds)")

        plt.ylim(-0.2, 1)
        plt.ylabel("Correlation coefficient (rho)")

        # plt.savefig("temporal.png", dpi=200)
        plt.show(block=True)
        plt.close()
