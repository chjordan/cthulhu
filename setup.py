# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from setuptools import setup
from codecs import open
from os import path
import re


here = path.abspath(path.dirname(__file__))
with open(path.join(here, "cthulhu/__init__.py")) as f:
    contents = f.read()
    version_number = re.search(r"__version__ = \"(\S+)\"", contents).group(1)

with open(path.join(here, "README.rst"), encoding="utf-8") as f:
    long_description = f.read()

setup(name="cthulhu",
      version=version_number,
      description="Python software suite for various ionospheric analyses",
      long_description=long_description,
      url="https://gitlab.com/chjordan/cthulhu",
      author="Christopher Jordan",
      author_email="christopherjordan87@gmail.com",
      license="MPL 2.0",
      keywords="signal processing",
      packages=["cthulhu"],
      scripts=["scripts/cthulhu_wrapper.py",
               "scripts/cthulhu_rts2format.py",
               "scripts/cthulhu_format_convertor.py"],
      install_requires=["numpy",
                        "scipy",
                        "astropy",
                        "matplotlib",
                        "pyGrad2Surf",
                        "pyyaml",
                        "future"],
      )
