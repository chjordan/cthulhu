#!/usr/bin/env python

# Python 2 and 3 compatibility
from __future__ import print_function, division

import numpy as np
import cthulhu.blacklist as blacklist


def test_get_blacklist():
    b = blacklist.get_blacklist()
    # Make sure this is a 1-dimensional list.
    assert len(np.shape(b)) == 1


def test_bad_sources():
    # Pass in no sources; we should get an empty list back.
    b = blacklist.bad_sources([])
    assert len(b) == 0

    # Pass in two bad sources and two dummies.
    b = blacklist.bad_sources(["MWA001656-25590",
                               "MWA001656-2559",
                               "asdf",
                               "MWA005549-2621"])
    assert len(b) == 4
    # Why doesn't this work?: b == [False, True, False, True]
    for i, x in enumerate([False, True, False, True]):
        assert b[i] == x


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
