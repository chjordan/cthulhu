#!/usr/bin/env python

# Python 2 and 3 compatibility
from __future__ import division

import numpy as np

from cthulhu.reconstruct import Obsid


def test_list_metric():
    # Here, we test predictably-generated Gaussian-distributed "ionospheric corrections"
    # to ensure the associate ionospheric quality metric is sensible.
    size = 1000
    np.random.seed(1000)
    ra = np.random.uniform(-5, 5, size=size)
    dec = np.random.uniform(-5, 5, size=size)
    ra_shifts = np.random.normal(scale=0.1/60, size=size)
    dec_shifts = np.random.normal(scale=0.1/60, size=size)

    obj = Obsid([ra, dec, ra_shifts, dec_shifts], blacklist=False, obsid=42)
    obj.obsid_metric()
    assert obj.metrics[0][0] < 0.12
    assert obj.metrics[1][0] < 0.53


def test_list_metric_with_sources():
    # Allow some additional code to run.
    size = 1000
    np.random.seed(1000)
    ra = np.random.uniform(-5, 5, size=size)
    dec = np.random.uniform(-5, 5, size=size)
    ra_shifts = np.random.normal(scale=0.1/60, size=size)
    dec_shifts = np.random.normal(scale=0.1/60, size=size)
    sources = np.arange(size).astype("|U4")
    flux_densities = np.arange(size)

    obj = Obsid([ra, dec, ra_shifts, dec_shifts],
                sources=sources, flux_densities=flux_densities)
    obj.obsid_metric()
    assert obj.metrics[0][0] < 0.12
    assert obj.metrics[1][0] < 0.53


def test_dict_metric():
    # This test is similar to test_list_metric, but we use a dict structure instead.
    # Take this opportunity to give the "ionosphere" a preferred direction.
    size = 1000
    np.random.seed(1000)
    ra = np.random.uniform(-5, 5, size=size)
    dec = np.random.uniform(-5, 5, size=size)
    ra_shifts = np.abs(np.random.normal(scale=0.1/60, size=size))
    dec_shifts = np.random.normal(scale=0.1/60, size=size)
    sources = np.arange(size).astype("|U4")

    # Populate the "data" dict.
    data = {
        "sources": {}
    }
    for i in range(len(ra)):
        data["sources"][sources[i]] = {
            "ra": ra[i],
            "dec": dec[i],
            "ra_shifts": ra_shifts[i],
            "dec_shifts": dec_shifts[i],
        }

    obj = Obsid(data)
    obj.obsid_metric()
    assert obj.metrics[0][0] < 0.12
    assert abs(obj.metrics[1][0] - 0.72) < 1e-3


def test_reconstruction():
    size = 100
    np.random.seed(1000)
    ra = np.random.uniform(-5, 5, size=size)
    dec = np.random.uniform(-5, 5, size=size)
    ra_shifts = np.abs(np.random.normal(scale=0.1/60, size=size))
    dec_shifts = np.random.normal(scale=0.1/60, size=size)

    obj = Obsid([ra, dec, ra_shifts, dec_shifts], radius=5)
    obj.reconstruct_tec()


def test_reconstruction_unfiltered():
    size = 100
    np.random.seed(1000)
    ra = np.random.uniform(-5, 5, size=size)
    dec = np.random.uniform(-5, 5, size=size)
    ra_shifts = np.abs(np.random.normal(scale=0.1/60, size=size))
    dec_shifts = np.random.normal(scale=0.1/60, size=size)

    obj = Obsid([ra, dec, ra_shifts, dec_shifts])
    obj.reconstruct_tec(filtering=False)
    obj.tec_power_spectrum()
    # obj.tec_residuals()
    obj.save_tec_fits()


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
